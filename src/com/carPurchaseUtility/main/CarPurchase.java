package com.carPurchaseUtility.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.carPurchaseUtility.entities.Car;
import com.carPurchaseUtility.entities.Customer;
import com.carPurchaseUtility.entities.Hyundai;
import com.carPurchaseUtility.entities.Maruti;
import com.carPurchaseUtility.entities.Toyota;
import com.carPurchaseUtility.util.SwitchCaseVariables;

public class CarPurchase implements SwitchCaseVariables {

	static Scanner scan = new Scanner(System.in);
	static int ch;
	static String[] args = null;
	static Map<Integer, Customer> customerMap = new HashMap<Integer, Customer>();
	static List<Car> carList = new ArrayList<Car>();

	public static void main(String[] args) {

		System.out.println("Welcome to ClickLabs Car Purchase Utility");
		System.out.println("__________________________________________");
		System.out.println("__________________________________________\n");
		while (true) {
			System.out.println("1.Add new customer.");
			System.out.println("2.Add new car to existing customer.");
			System.out.println("3.List all customers.");
			System.out.println("4.List customer based on id.");
			System.out.println("5.Generate prizes for customer.");
			System.out.println("6.Exit from application.\n");
			System.out.print("Enter your choice :");
			scan = new Scanner(System.in);
			if (!scan.hasNextInt()) {
				System.out.println("\n\nInvalid choice, try again.\n\n");
				continue;
			} else {
				ch = scan.nextInt();

				switch (ch) {

				case ADD_CUSTOMER:
					addCustomer();
					break;

				case ADD_CAR:
					Integer customerId = 0;
					do {

						System.out.print("\nEnter Customer id :");
						scan = new Scanner(System.in);
						if (!scan.hasNextInt()) {
							System.out
									.println("Special characters and alphabets not allowed in customer id.");
							continue;
						} else {
							customerId = scan.nextInt();
							addCar(customerId);
							break;
						}
					} while (!scan.hasNextInt());
					/*
					 * System.out.print("\nEnter Customer id :");
					 * if(!scan.hasNextInt()) {
					 * System.out.println("Oops not a valid customer id, try again."
					 * ); } else { customerId = scan.nextInt();
					 * addCar(customerId); }
					 */

					// while(addCar(customerId)==-1);
					break;

				case LIST_ALL_CUSTOMERS:
					if (customerMap.isEmpty()) {
						System.out
								.println("There are no customers to display.");
					} else {
						displayCustomers();
					}
					break;

				case LIST_CUSTOMER_BASED_ON_ID:
					System.out
							.print("\nEnter id of customer whose details you wish to see :");
					if (!scan.hasNextInt()) {
						System.out.println("Please enter a valid customer id");
					} else {
						customerId = scan.nextInt();
						displayCustomerBasedOnId(customerId);

					}
					break;

				case GENERATE_PRIZES:
					if (customerMap.size() < 6)
						System.out
								.println("There should be atleast 6 customers to generate prizes.");
					else {
						System.out.println("Enter any three customer id's");
						Integer customerId1 = scan.nextInt();
						Integer customerId2 = scan.nextInt();
						Integer customerId3 = scan.nextInt();
						generatePrizes(customerId1, customerId2, customerId3);
					}
					break;

				case EXIT:
					System.exit(0);

				default:
					System.out.println("You have entered an invalid option.");
				}

			}
		}

	}

	public static int addCustomer() {
		System.out.print("\nEnter customer id : ");
		scan = new Scanner(System.in);
		if (!scan.hasNextInt()) {
			System.out
					.println("Please enter a numeric customer id less than 20000000000.");
			addCustomer();
		} else {
			Integer customerId = scan.nextInt();
			if (customerMap.containsKey(customerId)) {
				System.out.println("This id already exists.");
				addCustomer();
			} else {
				System.out.print("Enter customer name :");
				scan = new Scanner(System.in);
				String name = scan.nextLine();
				customerMap.put(customerId, new Customer(name, customerId));
				System.out.println();
				System.out.println("Customer " + name + " with id "
						+ customerId + " sucessfully added.");
				System.out
						.println("___________________________________________________________\n\n");
			}
		}
		return 0;
	}

	public static int addCar(int customerId) {
		if (customerMap.containsKey(customerId)) {
			System.out.println("Select a Brand :");
			System.out.println("1.Hyundai");
			System.out.println("2.Maruti");
			System.out.println("3.Toyota");
			scan = new Scanner(System.in);
			// String brand=scan.nextLine().toLowerCase();
			if (!(scan.hasNextInt())) {
				System.out.println("Please enter a valid choice ");
				addCar(customerId);
			} else {
				int brand = scan.nextInt();
				if (!((brand == 1) || (brand == 2) || (brand == 3))) {
					System.out.println("Enter a valid choice");
					addCar(customerId);
				}
				if (brand == 1)
					System.out
							.println("Kindly enter the required details for Hyundai\n");
				else if (brand == 2)
					System.out
							.println("Kindly enter the required details for Maruti\n");
				else
					System.out
							.println("Kindly enter the required details for Toyota\n");

				scan = new Scanner(System.in);
				System.out.print("Enter Car id:");
				if (!scan.hasNextInt()) {
					System.out
							.println("Special characters and alphabets not allowed in car id, try again.");
				}

				else {
					int carID = scan.nextInt();
					System.out.print("Enter Model:");
					scan = new Scanner(System.in);
					String carModel = scan.nextLine();
					System.out.print("Enter Price:");
					if (!(scan.hasNextLong()))
						System.out.println("Please enter a valid price");
					else {

						long carPrice = scan.nextLong();
						if (carPrice < 150000) {
							System.out
									.println("\nPrize can't be less than 1.5 lakhs");
							addCar(customerId);
						} else {
							switch (brand) {

							case HYUNDAI:
								Hyundai hyundai = new Hyundai(carID, carPrice,
										carModel);
								customerMap.get(customerId).getCarList().add(
										hyundai);
								System.out
										.println(carModel
												+ " of brand Hyundai sucessfully added in "
												+ customerMap.get(customerId)
														.getName() + "'s list");
								System.out.println();
								break;

							case MARUTI:
								Maruti maruti = new Maruti(carID, carPrice,
										carModel);
								customerMap.get(customerId).getCarList().add(
										maruti);
								System.out
										.println(carModel
												+ " of brand Maruti sucessfully added in "
												+ customerMap.get(customerId)
														.getName() + "'s list");
								System.out.println();
								break;

							case TOYOTA:
								Toyota toyota = new Toyota(carID, carPrice,
										carModel);
								customerMap.get(customerId).getCarList().add(
										toyota);
								System.out
										.println(carModel
												+ " of brand Toyota sucessfully added in "
												+ customerMap.get(customerId)
														.getName() + "'s list");
								System.out.println();
								break;
							default:
								System.out.println("Not a valid brand");
								addCar(customerId);
								return 0;

							}

						}
					}
				}
			}
		} else {
			System.out.println("Customer does not exist");
			System.out.println();
		}
		return 0;
	}

	public static int displayCustomers() {

		List<Customer> customerList = new ArrayList<Customer>();
		for (Map.Entry<Integer, Customer> displayAllCustomers : customerMap
				.entrySet()) {
			customerList.add(displayAllCustomers.getValue());
		}
		Collections.sort(customerList, new Comparator<Customer>() {

			@Override
			public int compare(Customer c1, Customer c2) {
				return c1.getName().compareTo(c2.getName());
			}
		});
		for (Customer customer : customerList) {
			System.out.println(customer);
		}
		return 0;
	}

	public static int displayCustomerBasedOnId(int customerId) {
		if (customerMap.containsKey(customerId))
			System.out.println(customerMap.get(customerId));
		else
			System.out.println("Customer does not exist");
		return 0;
	}

	public static int generatePrizes(int customerId1, int customerId2,
			int customerId3) {
		List<Integer> customerIdList = new ArrayList<Integer>();
		for (Map.Entry<Integer, Customer> get_Id_Of_Customers : customerMap
				.entrySet()) {
			customerIdList.add(get_Id_Of_Customers.getKey());
		}
		Collections.shuffle(customerIdList);
		List<Integer> randomIdValues = customerIdList.subList(0, 6);
		List<Integer> enteredIdValues = new ArrayList<Integer>();

		enteredIdValues.add(customerId1);
		enteredIdValues.add(customerId2);
		enteredIdValues.add(customerId3);

		enteredIdValues.retainAll(randomIdValues);
		System.out.println("The winners are :\n\n");
		for (Integer winnerId : enteredIdValues) {
			displayCustomerBasedOnId(winnerId);
		}
		return 0;
	}
}
