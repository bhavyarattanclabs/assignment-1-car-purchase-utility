package com.carPurchaseUtility.util;

public interface SwitchCaseVariables {

	//Switch variables for main method class CarPurchase.java
	int ADD_CUSTOMER=1;
	int ADD_CAR=2;
	int LIST_ALL_CUSTOMERS=3;
	int LIST_CUSTOMER_BASED_ON_ID=4;
	int GENERATE_PRIZES=5;
	int EXIT=6;
	//Switch variables to select brand in addCar() function.
	int HYUNDAI=1;
	int MARUTI=2;
	int TOYOTA=3;

}
