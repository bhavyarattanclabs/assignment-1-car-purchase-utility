package com.carPurchaseUtility.entities;

public class Maruti extends Car{
	public Maruti(int id, long price, String model) {
		this.setCarId(id);
		this.setPrice(price);
		this.setModel(model);
		this.setResaleValue(price*6/10);
		this.setBrand("Maruti");

	}


}
