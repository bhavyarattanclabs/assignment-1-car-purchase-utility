package com.carPurchaseUtility.entities;

public class Toyota extends Car{

	public Toyota(int id, long price, String model) {
		this.setCarId(id);
		this.setPrice(price);
		this.setModel(model);
		this.setResaleValue(price*8/10);
		this.setBrand("Toyota");

	}

}
