package com.carPurchaseUtility.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Customer {

	private int id;
	private String name;
	private List<Car> carList=new ArrayList<Car>();;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Car> getCarList() {
		return carList;
	}

	public void setCarList(List<Car> carList) {
		this.carList = carList;
	}

	public Customer(String name, int id) {

		this.setName(name);
		this.setId(id);
	}

	public String toString()
	{
		StringBuilder builder=new StringBuilder();
		builder.append("Customer id : "+getId()+"\n");
		builder.append("Customer name : "+getName()+"\n");
		builder.append("Cars owned by "+getName()+" are :\n");
		builder.append("_______________________________________________\n");
		if(carList.size()>0){
			builder.append("Model\tCar Id\tBrand\tPrice\tResale value\n");
			builder.append("_______________________________________________\n");

			Collections.sort(carList, new Comparator<Car>() {


				@Override
				public int compare(Car car1, Car car2) {
					// TODO Auto-generated method stub
					return car1.getModel().compareTo(car2.getModel());
				}
			});

			for(int i=0;i<carList.size();i++){
		
			builder.append(carList.get(i).getModel()
					+"\t"+carList.get(i).getCarId()+"\t"+carList.get(i).getBrand()
					+"\t"+carList.get(i).getPrice()+"\t"+carList.get(i).getResaleValue()+"\n");
			builder.append("------------------------------------------\n");
			}			
		}
		else{
			builder.append("No Cars owned by customer "+getName()+" having customer id "+getId());
			builder.append("\n");
			builder.append("------------------------------------------\n");
			
		}

		return builder.toString();
	}
	
		
}
