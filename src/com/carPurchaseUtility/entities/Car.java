package com.carPurchaseUtility.entities;

public abstract class Car {
	
	private int carId;
	private String model;
	private String brand;
	private long price;
	private long resaleValue;

	public int getCarId() {
		return carId;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public void setCarId(int carId) {
		this.carId = carId;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public long getResaleValue() {
		return resaleValue;
	}
	public void setResaleValue(long resaleValue) {
		this.resaleValue = resaleValue;
	}
}
