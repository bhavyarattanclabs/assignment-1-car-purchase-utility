package com.carPurchaseUtility.entities;

public class Hyundai extends Car{

	public Hyundai(int id, long price, String model) {
		this.setCarId(id);
		this.setPrice(price);
		this.setModel(model);
		this.setResaleValue(price*4/10);
		this.setBrand("Hyundai");

	}


}
